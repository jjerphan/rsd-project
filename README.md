# Change Detection in Bi-temporal Polarimetric SAR Images 

[build](/../builds/artifacts/master/file/report/main.pdf?job=building-latex)

## About this project

Many temporal images can now be acquired on the same area using Sentinel sensors.
Change detection between different acquisitions is of particular interest for
many applications. The aim of this project is to develop a change detection
approach using polarimetric data (two complex channels) and a specific data
representation proposed in [1] and refined in [2].

See the slides of our project's presentation (in French): https://cloud.mines-paristech.fr/index.php/s/aXhZ2o5BM8fIBR0

The developed method will be applied on hydrological network survey.

[1] A novel framework for change detection in bi-temporal polarimetric SAR images, D. Pirrone et al., SPIE Remote Sensing, 2016.

[2] Advanced Methods for Change Detection in Multi-polarization and
Very-High Resolution Multitemporal SAR Images. Davide Pirrone. PhD thesis, International Doctorate School in
Information and Communication Technologies - University of Trento, 1 2019.

### Some results

![](./figs/Redon_res.png)
![](./figs/Sajnam_res.png)


## Structure of the repository

The project is organised as follows:

| File or folder        | Role                                                |
| --------------------- | --------------------------------------------------- |
| `code/`               | Our code base                                       |
| `notebooks/`          | Jupyter Notebooks for experiments                   |
| `data/`               | Data (content not synced)                           |
| `README.md`           | This file.                                          |
| `requirements.txt`    | Python dependencies                                 |
| `report             ` | Latex report                                        |


## How to use

1. Place images in the `data` folder
2. Install requirements:
```bash
pip install -r requirements.txt
```
3. Set settings accordingly in `code/settings.py`
4. Run `code/main.py`

At the end of the execution, the results will be present in the figs `figs`.

**Notes**: You can run `code/fix_guyane_aratai.py` to crop the image to remove the black part.
