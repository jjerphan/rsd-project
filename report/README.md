# Rapport

[build](/../builds/artifacts/master/file/report/main.pdf?job=building-latex)


## Sur la compilation du projet
La dernière version du PDF sur `master` est disponible [ici](/../builds/artifacts/master/file/report/main.pdf?job=building-latex).

Celle-ci est mise à jour à chaque version nouvelle fusion dans `master`.

### Figures dans le LaTeX

Règle générale: **Toutes les figures doivent être enregistrée dans le dossier
`figs/` quel que soit leur format.**

Les formats supportés actuellement sont:

 - pdf et png
 - svg

### Figure en pdf ou en png

 1. Mettre la figure dans le dossier `figs/`, par exemple: `figs/lapin.pdf`
 1. L'utiliser dans le document LaTeX sans mettre ni le dossier, ni l'extension:

    ```latex
    \includegraphics[width=.5\textwidth]{lapin}
    ```
