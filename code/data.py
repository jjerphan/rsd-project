# -*- coding: utf-8 -*-

import os
import warnings

import numpy as np
import rasterio
from scipy.io.matlab.mio import loadmat
from scipy.signal import convolve2d

from settings import GRD_FOLDER, SLC_FOLDER, data_parameters

warnings.filterwarnings("ignore", category=rasterio.errors.NotGeoreferencedWarning)

"""
* GeoTIFF read and write
* Extract metadata: datetime, RPC,
* Load images from given data format
"""


def readGTIFF(fname):
    """
    Reads an image file into a numpy array,
    returns the numpy array with dimensios (height, width, channels)
    The returned numpy array is always of type numpy.float

    Copyright (C) 2018, Gabriele Facciolo <facciolo@cmla.ens-cachan.fr>
    Copyright (C) 2018, Carlo de Franchis <carlo.de-franchis@ens-cachan.fr>
    """
    # read the image into a np.array
    with rasterio.open(fname, "r") as s:
        # print('reading image of size: %s'%str(im.shape))
        im = s.read()
    return im.transpose([1, 2, 0]).astype(np.float)


def writeGTIFF(im, fname, copy_metadata_from=None):
    """
    Writes a numpy array to a GeoTIFF, PNG, or JPEG image depending on fname extension.
    For GeoTIFF files the metadata can be copied from another file.
    Note that if  im  and  copy_metadata_from have different size,
    the copied geolocation properties are not adapted.

    Copyright (C) 2018, Gabriele Facciolo <facciolo@cmla.ens-cachan.fr>
    Copyright (C) 2018, Carlo de Franchis <carlo.de-franchis@ens-cachan.fr>
    """
    import rasterio
    import numpy as np

    # set default metadata profile
    p = {
        "width": 0,
        "height": 0,
        "count": 1,
        "dtype": "uint8",
        "driver": "PNG",
        "affine": rasterio.Affine(0, 1, 0, 0, 1, 0),
        "crs": rasterio.crs.CRS({"init": "epsg:32610"}),
        "tiled": False,
        "nodata": None,
    }

    # read and update input metadata if available
    if copy_metadata_from:
        x = rasterio.open(copy_metadata_from, "r")
        p.update(x.profile)

    # format input
    if len(im.shape) == 2:
        im = im[:, :, np.newaxis]

    # override driver and shape
    indriver = get_driver_from_extension(fname)
    if indriver and (indriver != p["driver"]):
        # print('writeGTIFF: driver override from %s to %s'%( p['driver'], indriver))
        p["driver"] = indriver or p["driver"]
        p["dtype"] = "float32"

    # if indriver == 'GTiff' and (p['height'] != im.shape[0]  or  p['width'] != im.shape[1]):
    #    # this is a problem only for GTiff
    #    print('writeGTIFF: changing the size of the GeoTIFF')
    # else:
    #    # remove useless properties
    #    p.pop('tiled')

    p["height"] = im.shape[0]
    p["width"] = im.shape[1]
    p["count"] = im.shape[2]

    with rasterio.open(fname, "w", **p) as d:
        d.write((im.transpose([2, 0, 1]).astype(d.profile["dtype"])))


def get_driver_from_extension(filename):
    """
    Copyright (C) 2018, Gabriele Facciolo <facciolo@cmla.ens-cachan.fr>
    Copyright (C) 2018, Carlo de Franchis <carlo.de-franchis@ens-cachan.fr>
    """
    import os.path

    ext = os.path.splitext(filename)[1].upper()
    if ext in (".TIF", ".TIFF"):
        return "GTiff"
    elif ext in (".JPG", ".JPEG"):
        return "JPEG"
    elif ext == ".PNG":
        return "PNG"
    return None


def load_grd_data(place="Sajnam"):
    """
    Load the pair of images of first and last date from GRD data
    stored as two GTIFF files.

    To be used given the data format of the project.

    :return: 2 2-channels images and their associated date as string
    """

    images = sorted(os.listdir(GRD_FOLDER / place))

    # Loading paramaters of images (which ones should be compared, which ones should be ignored)
    params = data_parameters[
        (data_parameters["Type"] == "GRD") & (data_parameters["Name"] == place)
    ]
    date1 = str(params.loc[:, "T1"].iloc[0])
    date2 = str(params.loc[:, "T2"].iloc[0])
    excluded_dates = str(params.loc[:, "Ignore"].iloc[0]).split(",")
    if date1 in excluded_dates:
        raise Exception(
            f"Date '{date1}' in '{place}' has corrupted data. Please select another date."
        )
    if date2 in excluded_dates:
        raise Exception(
            f"Date '{date2}' in '{place}' has corrupted data. Please select another date."
        )

    # The name of the place sometimes changes
    prefix = images[0].split("_")[0]

    path1_vv = GRD_FOLDER / place / f"{prefix}_vv_{date1}.tif"
    path1_vh = GRD_FOLDER / place / f"{prefix}_vh_{date1}.tif"

    path2_vv = GRD_FOLDER / place / f"{prefix}_vv_{date2}.tif"
    path2_vh = GRD_FOLDER / place / f"{prefix}_vh_{date2}.tif"

    X1 = np.concatenate([readGTIFF(path1_vv), readGTIFF(path1_vh)], axis=2)
    X2 = np.concatenate([readGTIFF(path2_vv), readGTIFF(path2_vh)], axis=2)

    return X1, X2, date1, date2


def multilook_image(im, L: int = 4):
    """
    Simple filter that computes the mean
    :param im: image to be multilooked
    :param L: number of looks used for multilooking the image. Should preferably not be a prime number
    :return:
    """

    # Finding h, w such that h * w = L
    w = int(np.ceil(np.sqrt(L)))
    while L % w != 0:
        w += 1
    h = int(L / w)

    filtre = np.ones((h, w)) / L

    # We perform the mean in intensity as this makes more
    # sense physically (i.e. computing mean on an energy)
    intensity_image = im ** 2
    mean_intensity_image = convolve2d(intensity_image, filtre, mode="same")[::h, ::w]

    return np.sqrt(mean_intensity_image)


def load_slc_data(place, L: int = 4):
    """
    Load the pair of images of first and last date from SLC data
    stored in a MatLab matrix.

    To be used given the data format of the project.
    
    :param L: number of looks used for multilooking the image. Should preferably not be a prime number
    :return: 2 2-channels images and their associated date as string
    """

    data = loadmat(SLC_FOLDER / place)

    # Loading paramaters of images (which ones should be compared, which ones should be ignored)
    params = data_parameters[
        (data_parameters["Type"] == "SLC") & (data_parameters["Name"] == place)
    ]
    idx1 = str(params.loc[:, "T1"].iloc[0])
    idx2 = str(params.loc[:, "T2"].iloc[0])
    excluded_dates = str(params.loc[:, "Ignore"].iloc[0]).split(",")
    if idx1 in excluded_dates:
        raise Exception(
            f"Date '{idx1}' in '{place}' has corrupted data. Please select another date."
        )
    if idx2 in excluded_dates:
        raise Exception(
            f"Date '{idx2}' in '{place}' has corrupted data. Please select another date."
        )

    date1 = "First image"
    date2 = "Last image"

    # Simple adaption to correct format of SLC data in the data structure
    key_vh = "images_choisiesVH" if "images_choisiesVH" in data.keys() else "imagesVH"
    key_vv = "images_choisiesVV" if "images_choisiesVV" in data.keys() else "imagesVV"

    im1vh = multilook_image(data[key_vh][:, :, int(idx1)], L=L)
    im1vv = multilook_image(data[key_vv][:, :, int(idx1)], L=L)

    im2vh = multilook_image(data[key_vh][:, :, int(idx2)], L=L)
    im2vv = multilook_image(data[key_vv][:, :, int(idx2)], L=L)

    X1 = np.concatenate([im1vv[:, :, None], im1vh[:, :, None]], axis=2)
    X2 = np.concatenate([im2vv[:, :, None], im2vh[:, :, None]], axis=2)

    return X1, X2, date1, date2
